const form_model = [
  {
    name: 'c1',
    label: 'Bentuk Telur',
    options: [
      { text: 'Sangat Kecil', value: 'sk', fuzz: [0, 0, 0.5] },
      { text: 'Lonjong', value: 'l', fuzz: [0, 0.5, 1.0] },
      { text: 'Bulat', value: 'b', fuzz: [0.5, 1.0, 1.0] }
    ]
  },
  {
    name: 'c2',
    label: 'Kebersihan Kulit',
    options: [
      { text: 'Kotor', value: 'k', fuzz: [0, 0, 1.0] },
      { text: 'Bersih', value: 'br', fuzz: [0, 0, 1.0] }
    ]
  },
  {
    name: 'c3',
    label: 'Ukuran Telur',
    options: [
      { text: 'Kecil', value: 'k', fuzz: [0, 0, 0.33] },
      { text: 'Sedang', value: 's', fuzz: [0, 0.33, 0.67] },
      { text: 'Besar', value: 'b', fuzz: [0.33, 0.67, 1.0] },
      { text: 'Jumbo', value: 'j', fuzz: [0.67, 1.0, 1.0] }
    ]
  },
  {
    name: 'c4',
    label: 'Kondisi Telur',
    options: [
      { text: 'Pecah', value: 'p', fuzz: [0, 0, 0.5] },
      { text: 'Retak', value: 'r', fuzz: [0, 0.5, 1.0] },
      { text: 'Utuh', value: 'u', fuzz: [0.5, 1.0, 1.0] }
    ]
  },
  {
    name: 'c5',
    label: 'Warna Cangkang',
    options: [
      { text: 'Coklat', value: 'ct', fuzz: [0, 0, 0.5] },
      { text: 'Coklat Tua', value: 'cm', fuzz: [0, 0.5, 1.0] },
      { text: 'Coklat Muda', value: 'p', fuzz: [0.5, 1.0, 1.0] }
    ]
  }
];

const formatted = form_model.reduce((acc, model) => {
  acc[model.name] = s => {
    for (let opt of model.options) {
      if (opt.value == s) return opt.text;
    }
    throw new Error(`Can't find text for ${s} in kriteria ${model.label}`);
  };
  return acc;
}, {});

const priority_options = [
  { text: 'Sangat Rendah', value: 'sr' },
  { text: 'Rendah', value: 'r' },
  { text: 'Cukup', value: 'c' },
  { text: 'Tinggi', value: 't' },
  { text: 'Sangat Tinggi', value: 'st' }
];

function map_priority(s) {
  switch (s) {
    case 'sr': return [0, 0, 0.25];
    case 'r': return [0, 0.25, 0.5];
    case 'c': return [0.25, 0.5, 0.75];
    case 't': return [0.5, 0.75, 1.0];
    case 'st': return [0.75, 0.75, 1.0];
    default:
      throw new Error('Roar!!');
  }
}

function map_kriteria (item) {
  let results = [];
  for (let model of form_model) {
    let raw = item[model.name]
    let opt = model.options.find(({ value }) => value == raw)
    results.push(opt.fuzz);
  }
  return results;
}

function format_v(v) {
  if (v < 0.2) return 'Sangat Rendah';
  if (0.2 < v && v < 0.4) return 'Rendah';
  if (0.4 < v && v < 0.6) return 'Cukup';
  if (0.6 < v && v < 0.8) return 'Baik';
  if (0.8 < v) return 'Sangat Baik';
}

module.exports = {
  form_model,
  formatted,
  priority_options,
  map_priority,
  map_kriteria,
  format_v
};
