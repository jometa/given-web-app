const fmulter = require('fastify-multer');
const upload = fmulter();

module.exports = async function (fastify, opts) {

  const collection = fastify.mongo.db.collection('users');

  fastify.get('/', async function (request, reply) {
    reply.view('admin/login.pug');
  });

  fastify.get('/logout', async function (request, reply) {
    reply.clearCookie('fsaw.username')
      .redirect('/');
  });

  fastify.route({
    url: '/',
    method: 'POST',
    preHandler: upload.none(),
    handler: async function (request, reply) {
      const item = collection.findOne({
        username: request.body.username,
        password: request.body.password
      });
      reply.setCookie('fsaw.username', item.username)
        .redirect('/admin/data');
    }
  });

};

