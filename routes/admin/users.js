const fmulter = require('fastify-multer');
const upload = fmulter();
const ObjectID = require('mongodb').ObjectID;

module.exports = async function (fastify, opts) {
  const collection = fastify.mongo.db.collection('users');

  fastify.get('/', async function (request, reply) {
    const result = await collection.find();
    const items = await result.toArray();
    reply.view('admin/users/list.pug', {
      title: 'Daftar Users',
      subtitle: 'Menampilkan Daftar Users',
      items
    });
  });

  fastify.get('/create', function (request, reply) {
    reply.view('admin/users/create.pug', {
      title: 'Tambah user',
      subtitle: 'Form Input Data User'
    });
  });

  fastify.route({
    method: 'POST',
    url: '/create',
    preHandler: upload.none(),
    handler: async function (request, reply) {
      await collection.insertOne(request.body);
      reply.redirect('/admin/users/');
    }
  });

  fastify.get('/delete/:id', async function (request, reply) {
    const id = request.params.id;
    await collection.deleteOne({
      _id: new ObjectID(id)
    });
    reply.redirect('/admin/users');
  });

  fastify.get('/:id', async function (request, reply) {
    const id = request.params.id;
    const selector = {
      id: new ObjectID(id)
    };
    const result = await collection.findOne(selector);
    reply.view('admin/users/edit.pug', {
      item: result,
      title: 'Edit user',
      subtitle: 'Form Input Data User'
    });
  });

  fastify.post('/:id', async function (request, reply) {
    const id = request.params.id;
    const selector = {
      id: new ObjectID(id)
    };
    await collection.update(selector, request.body);
    reply.redirect('/admin/users');
  });
};

