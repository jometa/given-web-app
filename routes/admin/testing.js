const model = require('../../services/model');
const fsaw = require('../../services/fsaw');
const fmulter = require('fastify-multer');
const upload = fmulter();

module.exports = async function(fastify, opts) {

  const settingsCollection = fastify.mongo.db.collection('settings');
  const candidatesCollection = fastify.mongo.db.collection('candidates');
  
  fastify.get('/', async function (request, reply) {
    const result = await settingsCollection.findOne({ key: 'priority' });
    const { key, ...item } = result;
    reply.view('admin/testing/test-form.pug', {
      title: 'Pengujian Sensitivitas',
      subtitle: 'Form Input Bobot',
      form_model: model.form_model,
      formatted: model.formatted,
      priority_options: model.priority_options,
      item
    });
  });

  fastify.route({
    url: '/',
    method: 'POST',
    preHandler: upload.none(),
    handler: async function (request, reply) {
      const prios = request.body;
      const result = await fsaw.sensitivity({ db: fastify.mongo.db, prios });
      reply.view('admin/testing/test-result.pug', {
        title: 'Pengujian Sensitivitas',
        subtitle: 'Hasil Pengujian',
        ...result
      });
    }
  });

};