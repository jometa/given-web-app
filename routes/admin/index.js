const data_plugin = require('./data');
const kriteria_plugin = require('./kriteria');
const rank_plugin = require('./rank');
const report_plugin = require('./report');
const users_plugin = require('./users');
const testing_plugin = require('./testing');

 module.exports = async function(fastify, opts) {

  fastify
    .addHook('onRequest', async (request, reply) => {
      const username = request.cookies['fsaw.username'];
      if (!username) {
        reply.redirect('/auth');
      }
    })
    .register(data_plugin, { prefix: 'data' })
    .register(kriteria_plugin, { prefix: 'kriteria' })
    .register(rank_plugin, { prefix: 'rank' })
    .register(report_plugin, { prefix: 'report' })
    .register(users_plugin, { prefix: 'users' })
    .register(testing_plugin, { prefix: 'testing' });

};
